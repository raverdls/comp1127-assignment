#Introduction To COmputing 2 Assignment
# Phillip Llewellyn & Lateefah Smellie

"""
Group Information:
Phillip Llewellyn : IDNUM
Lateefah Smellie: IDNUMBER
"""
# Global Variables
availabilityQueue_UWI = None
availabilityQueue_Papine = None
availabilityQueue_Liguanea = None
availabilityQueue_HalfWayTree = None
knownPassengers = {4444444: 1}
fare = 100

# Auxilary Graphics
def cls():
        for i in range(50):
            print()


############################
#                          #
#        Question 1        #
#                          #
############################

#constructor for the Driver Abstract Data Type
def driver_make(firstName, lastName, carMakeAndModel):
    return("Driver",[firstName, lastName, carMakeAndModel,0])

#Accessors mutators and predicate for the Driver abstract Data type

#driver Details accessor
def getDriverInfo(Driver):
    return Driver[1]
#Firstname accessor
def driver_getFirstName(Driver):
    return str(getDriverInfo(Driver)[0])

#Firstname accessor
def driver_getLastName(Driver):
    return str(getDriverInfo(Driver)[1])

#Car Make And Model accessor
def driver_getCarMakeAndModel(Driver):
    return str(getDriverInfo(Driver)[2])

#Number Of Trips Completed accessor
def driver_getNumberOfTripsCompleted(Driver):
    return int(getDriverInfo(Driver)[3])

# Mutator to Increase the Trip Completed
def driver_increaseTripsCompleted(Driver):
    if driver_isDriver(Driver):
        Trips = getDriverInfo(Driver)
        Trips[3] +=1
def driver_isDriver(Driver):
    if type(Driver) == tuple and Driver[0] == "Driver":
        return True
    return False

#Predicate to test of driver is new for the Driver ADT
def driver_isNewDriver(Driver):
    if driver_isDriver(Driver) and driver_getNumberOfTripsCompleted(Driver) == 0:
        return True
    return False

############################
#                          #
#        Question 2        #
#                          #
############################

'''Driver Availability Queue Abstract Data Type'''

#constructor for the Availability Queue ADT
def availabilityQueue_make(LocationName):
    return('AvailabilityQueue', LocationName, [])

'''Accessors'''

#gets driver location
def availabilityQueue_getLocationName(AvailabilityQueue):
    return AvailabilityQueue[1]

#gets drivers in queue
def availabilityQueue_getDrivers(AvailabilityQueue):
    return AvailabilityQueue[2]

#gets driver at front of queue
def availabilityQueue_front(AvailabilityQueue):
    if not availabilityQueue_isAvailabilityQueue(AvailabilityQueue):
        #raise TypeError (AvailabilityQueue, ' is not an Availability Queue')
        raiseError(TypeError, ' is not an Availability Queue', AvailabilityQueue)

    elif availabilityQueue_isEmpty(AvailabilityQueue):
        #raise Error('Availability Queue is empty')
        raiseError(ValueError, 'Availability Queue is empty')

    else:
        return availabilityQueue_getDrivers(AvailabilityQueue)[0]

'''Mutators'''

#adds driver to back of queue
def availabilityQueue_enqueue(AvailabilityQueue, Driver):

    if not availabilityQueue_isAvailabilityQueue(AvailabilityQueue):
        raiseError(TypeError, ' is not an Availability Queue', AvailabilityQueue)

    else:
        availabilityQueue_getDrivers(AvailabilityQueue).append(Driver)


#removes driver from front of queue
def availabilityQueue_dequeue(AvailabilityQueue):
    if not availabilityQueue_isAvailabilityQueue(AvailabilityQueue):
        raiseError(TypeError, ' is not an Availability Queue', AvailabilityQueue)

    elif availabilityQueue_isEmpty(AvailabilityQueue):
        raiseError(ValueError, 'Availability Queue is empty')

    else:
        availabilityQueue_getDrivers(AvailabilityQueue).pop(0)


'''Predicates'''

#checks if argument is Availability Queue
def availabilityQueue_isAvailabilityQueue(AvailabilityQueue):
    correct_aq_type = type(AvailabilityQueue) == tuple
    correct_location_type = type(availabilityQueue_getLocationName(AvailabilityQueue)) == str
    correct_driver_type = type(availabilityQueue_getDrivers(AvailabilityQueue)) == list
    correct_tag_type = AvailabilityQueue[0] == 'AvailabilityQueue'

    return correct_aq_type and correct_location_type and correct_driver_type and correct_tag_type


#checks if Availability Queue is empty
def availabilityQueue_isEmpty(AvailabilityQueue):
    if availabilityQueue_isAvailabilityQueue(AvailabilityQueue):
        return len(availabilityQueue_getDrivers(AvailabilityQueue)) == 0


'''Function to raise errors'''
def raiseError (ErrType, Msg, Obj=None):
    if (Obj == None):
        print(Msg)
    else:
        print(Obj, Msg)


############################
#                          #
#        Question 3        #
#                          #
############################

availabilityQueue_UWI = availabilityQueue_make("UWI")
availabilityQueue_Papine = availabilityQueue_make("Papine")
availabilityQueue_Liguanea = availabilityQueue_make("Liguanea")
availabilityQueue_HalfWayTree = availabilityQueue_make("Half-Way-Tree")
availabilityQueue_List = [availabilityQueue_UWI, availabilityQueue_Papine, availabilityQueue_Liguanea, availabilityQueue_HalfWayTree]
Locations = [availabilityQueue_getLocationName(que) for que in availabilityQueue_List ]

############################
#                          #
#        Question 4        #
#                          #
############################

def calculateDiscount(PassengerTelephoneNumber):
    if PassengerTelephoneNumber in knownPassengers :
        num_fail = knownPassengers[PassengerTelephoneNumber]
        if num_fail > 0:
            return 0.10 * num_fail
    return 0.0

############################
#                          #
#        Question 5        #
#                          #
############################

def calculateFare(StartLocation, EndLocation, PassengerTelephoneNumber):
    discount = 0.0
    discount = fare * calculateDiscount(PassengerTelephoneNumber)
    return fare - discount


############################
#                          #
#        Question 6        #
#                          #
############################

def moveTaxi(startLocation, endLocation):
    Locations = [availabilityQueue_getLocationName(que) for que in availabilityQueue_List ]
            #check if drivers are available at the start location and remove the driver
    if not availabilityQueue_isEmpty(availabilityQueue_List[Locations.index(startLocation)]):
        driver = availabilityQueue_front(availabilityQueue_List[Locations.index(startLocation)])
        availabilityQueue_dequeue(availabilityQueue_List[Locations.index(startLocation)])
        driver_increaseTripsCompleted(driver)
            #append the new driver to the new availability queue
        availabilityQueue_enqueue(availabilityQueue_List[Locations.index(endLocation)],driver)

############################
#                          #
#        Question 7        #
#                          #
############################

def requestTaxi(PassengerTelephoneNumber, PassengerLocation, PassengerDestination):
    if PassengerLocation == PassengerDestination:
        print("Start and end locations are the same!")
    else:
        if PassengerLocation in Locations and PassengerDestination in Locations:
            # check if the locations are valid and calculate the fare and display it to the usere
            print("====================================")
            print("The cost of this trip is ", calculateFare(PassengerLocation, PassengerDestination, PassengerTelephoneNumber))
            print("====================================")
            confirmation = input('Enter "Y" to confirm the trip or "N" to cancel -  \n')
            if confirmation == "Y":
                #check to ensure that drivers are available
                if not availabilityQueue_isEmpty(availabilityQueue_List[Locations.index(PassengerLocation)]):
                    #driver_increaseTripsCompleted(availabilityQueue_front(availabilityQueue_List[Locations.index(PassengerLocation)]))
                    moveTaxi(PassengerLocation,PassengerDestination)
                    knownPassengers[PassengerTelephoneNumber] = 0
                else:
                    print("No driver available \n")
                    if not PassengerTelephoneNumber in knownPassengers:
                        knownPassengers[PassengerTelephoneNumber] = 1
                    else:
                        knownPassengers[PassengerTelephoneNumber] += 1
        else:
            print("ERROR: You have entered invalid location/ destination, Try again \n")


############################
#                          #
#        Question 8        #
#                          #
############################

def showDailyReport():
    for queue in availabilityQueue_List:
        for driver in availabilityQueue_getDrivers(queue):
            print('Driver: ', driver_getFirstName(driver) + ' ' + driver_getLastName(driver) , '\nTrips Completed: ', str(driver_getNumberOfTripsCompleted(driver)), '\n')


def showNextDayAvailability():
    for queue in availabilityQueue_List:
        if not availabilityQueue_isEmpty(queue):
            driver = availabilityQueue_front(queue)
            print(availabilityQueue_getLocationName(queue) + ' : ' + driver_getFirstName(driver) + ' ' + \
                  driver_getLastName(driver) + ' ' + driver_getCarMakeAndModel(driver))
def requestservice():
        print("=====================================================\n")
        print("Heres a list of our Available Drivers: ")
        print("=====================================================\n")
        showNextDayAvailability()
        print("=====================================================\n")
        showLocations()
        request = input('\nEnter "Y" to request a taxi or "N" to end use of the service for that period - \n')

        while not (request == "N" or request == "n"):
                if request == "Y" or request == "y":
                        TelephoneNumber = input("Please enter your Telephone Number: \n")
                        Location  = input("Please enter your current location: \n")
                        Destination  = input("Please enter your destination: \n")
                        print("\n\n\tYour request is being made... Wait a moment !\n")
                        requestTaxi(TelephoneNumber, Location, Destination)
                        input ("Press any key to continue...")
                        cls()
                        print("=====================================================\n")
                        print("Heres a list of our Available Drivers: ")
                        print("=====================================================\n")
                        showNextDayAvailability()
                        print("=====================================================\n")
                        request = input('\nEnter "Y" to request a taxi or "N" to end use of the service for that period - \n')

        print()
        endService()

def addDriver():
	print("\tPlease enter driver information. \n\n")
	firstName = input("Enter Driver firstname:  \n")
	lastName = input("Enter Driver lastName:  \n")
	carMakeAndModel = input("Please Enter the make model for the car:  \n")
	location = input("Please Enter The location where the driver is available:  \n")
	print("Thank you for that information , Validating...")
	driver  = driver_make(firstName, lastName, carMakeAndModel)
	if location in Locations:
		availabilityQueue_enqueue(availabilityQueue_List[Locations.index(location)],driver)
		print("Thank you your Driver was added!\n")
		input ("Press any key to continue...")
	else:
		print("Please try again, location not in our system\n")
		input ("Press any key to continue...")

def addlocation():
        print("\t Please enter your location info\n")
        nlocation = input("Enter the new location for taxi:  \n")
        if nlocation in Locations:
                print('\n\t ', nlocation, ' already eists.\n')
        else:
                print('\n\tAdding ', nlocation, ' to the list of available locations.')
                availabilityQueue_List.append(availabilityQueue_make(nlocation))
                Locations.append(nlocation)
                print('done!!!\n')
                showLocations()
        input('Press any key to continue...')

def showLocations():
        print('\nThe available locations are: \n')
        for location in Locations:
                print('\tlocation\n')

def endService():
    cls()
    print("=====================================================")
    print("Drivers Report ")
    print("=====================================================\n")
    showDailyReport()
    print("=====================================================\n\n")
    input ("Press any key to continue...")
    cls()
    print("=====================================================")
    print("Next Available Driver at Each location")
    print("=====================================================\n")
    showNextDayAvailability()
    print("=====================================================\n")
    print("\t\t\t THANK YOU FOR USING OUR SERVICE")

def youba():
	option  = ""
	while not option == "Q":
		cls()
		print("\t\t=====================================================")
		print("\t\t=                                                   =")
		print("\t\t=        Welcome To Youba Taxi Services             =")
		print("\t\t=                                                   =")
		print("\t\t=====================================================\n\n")
		print ('\t+----------------------------------------------------------------+')
		print ('\t|                                                                |')
		print ('\t|    1.     Request A Taxi                                       |')
		print ('\t|    2.     List All Drivers                                     |')
		print ('\t|    3.     Add A driver                                         |')
		print ('\t|    4.     Add another location                                 |')
		print ('\t|    Q.     Quit                                                 |')
		print ('\t|                                                                |')
		print ('\t+----------------------------------------------------------------+')
		option = input ("\tEnter a selection [1-4 or Q] : ")


		if option  == "2":
			cls()
			print("=====================================================")
			print("Drivers Report ")
			print("=====================================================\n")
			showDailyReport()
			print("=====================================================\n\n")
			input ("Press any key to continue...")
		elif option  == "3":
			cls()
			addDriver()
		elif option  == "4":
			cls()
			addlocation()
		elif option  == "1":
			cls()
			requestservice()
		elif option  =="Q":
			endService()
			print("Thank You for using the sevice, Bye!!")

if __name__ == '__main__':
    print("==================================================================\n")
    print("                             Test Data")
    print("==================================================================\n\n")
    a = driver_make("Shawn","Feynmann","MercedesM500")
    b = driver_make("Mina","Linton","ToyotaCorolla")
    e = availabilityQueue_make('Kingston')
    print(availabilityQueue_List)
    availabilityQueue_enqueue(availabilityQueue_UWI,a)
    availabilityQueue_enqueue(availabilityQueue_UWI,b)
    print(availabilityQueue_UWI)
    moveTaxi("UWI", "Papine")
    print(availabilityQueue_UWI)
    print(availabilityQueue_Papine)
   # requestTaxi(4444444, "Liguanea", "Half-Way-Tree")
    print(availabilityQueue_List)
    print(knownPassengers)
    print("==================================================================\n\n")
    #Run the youba service
    youba()
